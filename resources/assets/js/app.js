import $ from 'jquery';

$('.js-listing-filter').on('change', function () {
  const area = $(this).val();
  document.location.href = area ? `/?service-area=${area}#listing` : '/#listing';
});


// prevent modal close on click inside modal
$('.js-modal-interior').click(function(e) { e.stopPropagation()});