<?php
    use function Tonik\Theme\App\template;
    $postsPerPage = -1;
    $pageNumber = isset($_GET['pg']) ? $_GET['pg'] : 1;
    $serviceAreas = [];
    if (isset($_GET['service-area'])){
        $serviceAreas =  [get_post($_GET['service-area'])];
    }
    if (isset($organization)){
        $serviceAreas = get_field('service_areas', $organization->ID);
    }
    $metaQuery = [];
    if ($serviceAreas){
        $metaQuery = array(
            'relation' => 'OR',
        );
        foreach($serviceAreas as $serviceArea){
            $metaQuery[]=  array(
                    'key' => 'service_area',
                    'value' =>$serviceArea->ID
                );
        }
    }

    $restaurants = new WP_Query(array(
        'post_type' => 'restaurants',
        'posts_per_page' => $postsPerPage,
        'paged' => $pageNumber,
        'meta_query' => $metaQuery
    ));
    $lastPage = ceil($restaurants->found_posts / $postsPerPage);
?>

<section class="listing" id="listing">
    <div class="container">
        <?php if (!isset($organization)) { ?>
            <?php if ($serviceArea) {
                $serviceAreaPost = get_posts(array(
                    'post_type' => 'service_areas',
                    'p' => $serviceArea
                ));
            ?>
                <h3 class="listing__title">Restaurants in <?= $serviceAreaPost[0]->post_title; ?></h3>
            <?php } else { ?>
                <h3>All Restaurants</h3>
            <?php } ?>
        <?php } ?>
        <?php if (!isset($organization)){ ?>
        <?php template('components/listing-filter'); ?>
        <?php } ?>
        <div class="listing__content">
            <?php if (count($restaurants->posts) === 0) { ?>
                <h4>No restaurants match your search. Try again!</h4>
            <?php } ?>
            <?php foreach($restaurants->posts as $restaurant) {
                $area = get_field('service_area', $restaurant->ID);
                $organizations = array();
                if (isset($organization)){
                    $organizations = array($organization);
                } else {
                    if ($area){
                        $organizations = get_posts(array(
                            'post_type'=>'organizations',
                            'meta_query' => array(
                                array(
                                    'key' => 'service_areas',
                                    'value' => '"' . $area->ID . '"',
                                    'compare' => 'LIKE'
                                )
                            )
                        ));
                    }
                } ?>
                <div class="card">
                    <div class="card-section">
                        <h4><?php echo get_the_title($restaurant->ID); ?></h4>
                        <div class="card__meta">
                            <?php echo $area->post_title ?>
                            <p><?php echo get_field('address', $restaurant->ID); ?></p>
                        </div>
                        <p><a class="button" data-open="Modal<?php echo $restaurant->ID; ?>">Buy Gift Card</a></p>
                    </div>
                    <div class="card__modal reveal" data-reveal id="Modal<?php echo $restaurant->ID; ?>" data-close>
                        <div class="card__modal-interior js-modal-interior">
                            <h4><?php echo get_the_title($restaurant->ID); ?> Gift Card Link:</h4>
                            <p><a onclick='captureOutboundLink("<?php echo get_field('gift_card_link', $restaurant->ID); ?>");' taget="_blank" href="<?php echo get_field('gift_card_link', $restaurant->ID); ?>" target="_blank"><?php echo get_field('gift_card_link', $restaurant->ID); ?></a></p>
                            <?php if ($organizations){ ?>
                                <p>When purchasing, send the gift card to <?php if (count($organizations) > 1){?>one of <?php } ?>the following email<?php if (count($organizations) > 1){?>s<?php } ?>:</p>
                                <ul>
                                <?php
                                    foreach ($organizations as $organization) {
                                    $email = get_field('contact_email', $organization->ID);
                                    if ($email) {
                                        echo '<li><input type="text" value="' . $email . '"><span>Partner: ' . $organization->post_title . '</span></li>';
                                    }
                                }
                                ?>
                                </ul>
                                <p>who will distribute it someone in need in <?php echo $area->post_title; ?>.</p>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <?php /* if ($restaurants->found_posts > $postsPerPage) { ?>
            <div class="listing__pagination">
                <nav aria-label="Pagination">
                    <ul class="pagination">
                        <?php $urlBase = $serviceArea ? '/?service-area=' . $serviceArea . '&' : '/?'; ?>
                        <li class="pagination-previous <?= $pageNumber == 1 ? 'disabled' : '' ?>"><a href="<?= $urlBase; ?>" aria-label="First page">First page</a></li>
                        <?php if ($pageNumber - 1 > 0) { ?>
                            <li><a href="<?= $urlBase . 'pg=' . ($pageNumber - 1) . '#listing' ?>" aria-label="Page <?= $pageNumber - 1 ?>"><?= $pageNumber - 1 ?></a></li>
                        <?php } ?>
                        <li class="current"><a href="#" aria-label="Page <?= $pageNumber ?>"><?= $pageNumber ?></a></li>
                        <?php if ($pageNumber * $postsPerPage < $restaurants->found_posts) {?>
                            <li  <?= $pageNumber == $lastPage ? 'disabled' : ''?>><a href="<?= $urlBase . 'pg=' . ($pageNumber + 1) . '#listing' ?>" aria-label="Page <?=$pageNumber + 1?>"><?=$pageNumber + 1?></a></li>
                        <?php }?>
                        <li class="pagination-next <?= $pageNumber == $lastPage ? 'disabled' : '' ?>"><a href="<?= $urlBase . 'pg=' . $lastPage . '#listing' ?>" aria-label="Last page">Last page</a></li>
                    </ul>
                </nav>
            </div>
        <?php } */ ?>
    </div>
</section>

<script>
    /**
     * Function that captures a click on an outbound link in Analytics.
     * This function takes a valid URL string as an argument, and uses that URL string
     * as the event label. Setting the transport method to 'beacon' lets the hit be sent
     * using 'navigator.sendBeacon' in browser that support it.
     */
    var captureOutboundLink = function(url) {
        gtag('event', 'click', {
            'event_category': 'outbound',
            'event_label': url,
            'transport_type': 'beacon'
        });
    }
</script>
