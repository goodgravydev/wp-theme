<?php
    $serviceAreas = get_posts(array(
        'post_type'      => 'service_areas',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'ASC'
    ));
?>

<section class="listing-filter">
    <div class="container">
        <label>Choose an Area of MSP
            <select class="js-listing-filter">
                <option value="">All areas</option>
                <?php foreach ($serviceAreas as $serviceArea) {
                    $restaurants = new WP_Query(array(
                        'post_type' => 'restaurants',
                        'meta_query' =>
                            array(
                                array(
                                'key' => 'service_area',
                                'value' =>$serviceArea->ID
                            )
                        )
                    ));
                    if ($restaurants->post_count > 0) {
                ?>
                    <option value="<?= $serviceArea->ID; ?>" <?php echo isset($_GET['service-area']) && $_GET['service-area'] == $serviceArea->ID ? 'selected' : '' ?>><?= $serviceArea->post_title; ?></option>
                <?php } } ?>
            </select>
        </label>
    </div>
</section>
