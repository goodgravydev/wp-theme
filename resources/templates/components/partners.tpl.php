<?php
   $organizations = get_posts('post_type=organizations&posts_per_page=-1');
?>

<div class="partners">
    <div class="container">
        <h3 class="partners__title">Community Partners</h3>
        <div class="partners__content">
            <?php foreach ($organizations as $organization){ ?>
                <img src="<?php echo get_field('logo', $organization->ID); ?>">
            <?php } ?>
        </div>
    </div>
</div>
