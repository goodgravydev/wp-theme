<?php
    use function Tonik\Theme\App\template;

    $title       = get_sub_field('title');
    $description = get_sub_field('description');
    $bgImage = get_sub_field('background_image');
?>
<section class="hero" style="background-image: url(<?= $bgImage ?>);">
    <div class="container">
        <div class="hero__content">
            <h1><?= $title ?></h1>
            <?php if ($description) { echo $description; } ?>
        </div>
        <div class="hero__steps">
            <div class="step">
                <img src="<?php echo get_sub_field('step_one_image'); ?>">
                <p><?php echo get_sub_field('step_one_copy'); ?></p>
            </div>
            <div class="step">
                <img src="<?php echo get_sub_field('step_two_image'); ?>">
                <p><?php echo get_sub_field('step_two_copy'); ?></p>
            </div>
        </div>
        <?php template('components/search'); ?>
    </div>
</section>
