<?php

use function Tonik\Theme\App\template;

?>
<section class="form">
    <div class="container">
        <?php Ninja_Forms()->display( 2 ); ?>
    </div>
</section>
