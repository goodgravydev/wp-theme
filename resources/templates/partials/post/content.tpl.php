<?php
    use function Tonik\Theme\App\template;
    $serviceAreas = get_field('service_areas');
?>

<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v6.0"></script>

<div class="container">
    <article>
        <div class="article-header">
            <h1><?php the_title(); ?></h1>
        </div>
        <div class="article-content">
            <p>Our beloved restaurants are closing their doors & our neighbors are experiencing food insecurity. <br/>Let’s do our part in making this right.</p>
            <p>Buy a gift card online from one of the restaurants below, and then share it via email to <a><?php echo get_field('contact_email'); ?></a>, to distribute to someone in need.</p>
        </div>
    </article>
    <?php template('components/listing', ['organization'=> get_post()])?>

</div>
