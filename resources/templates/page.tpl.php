<?php
use function Tonik\Theme\App\template;

get_header();
?>
    <div class="content">
        <?php if (have_posts()): ?>
            <?php while (have_posts()): the_post()?>
                <?php
                    if (have_rows('components')) {
                        while (have_rows('components')) {
                            the_row();
                            template('components/' . get_row_layout());
                        }
                    }
                ?>
            <?php endwhile;?>
        <?php endif;?>
    </div>

<?php get_footer();?>
