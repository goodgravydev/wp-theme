            <footer class="footer">
                <div class="container">
                    Built by <a href="https://goodgravy.digital" target="_blank">Good Gravy</a>.
                </div>
            </footer>
        </main>

        <?php wp_footer(); ?>
    </body>
</html>
