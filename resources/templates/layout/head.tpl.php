<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/assets/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/assets/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/assets/images/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/resources/assets/images/site.webmanifest">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-160268644-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-160268644-1');
    </script>
    <meta property="og:image:url" content="<?php echo get_stylesheet_directory_uri(); ?>/resources/assets/images/support-msp.png" />
    <meta name="twitter:image" content="<?php echo get_stylesheet_directory_uri(); ?>/resources/assets/images/support-msp-square.png" />
    <meta name="twitter:card" content="summary" />
    <meta property="og:url" content="https://supportmsp.com" />
    <meta property="og:title" content="Together, We Commit To Supporting Our People" />
    <meta property="og:description" content="Our beloved local restaurants need our help. Our neighbors, friends, and colleagues need us now more than ever. Add restaurants, buy gift cards, order take out, and support our local community." />
</head>
<body <?php body_class(); ?>>
<main id="app" class="app">
    <div class="container">
        <nav class="">
            <div class="top-bar">
                <div class="top-bar-left">
                    <a href="<?= get_home_url(); ?>"><img
                            src="<?php echo get_stylesheet_directory_uri(); ?>/resources/assets/images/support-msp.svg"></a>
                </div>
                <div class="top-bar-right">
                    <a class="button" href="/submit-restaurant">Submit Restaurant</a>
                </div>
            </div>
    </div>
    </nav>
</main>
