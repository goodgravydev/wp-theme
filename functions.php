<?php

/*
 |------------------------------------------------------------------
 | Bootstraping a Theme
 |------------------------------------------------------------------
 |
 | This file is responsible for bootstrapping your theme. Autoloads
 | composer packages, checks compatibility and loads theme files.
 | Most likely, you don't need to change anything in this file.
 | Your theme custom logic should be distributed across a
 | separated components in the `/app` directory.
 |
 */

// Require Composer's autoloading file
// if it's present in theme directory.
if (file_exists($composer = __DIR__ . '/vendor/autoload.php')) {
    require $composer;
}

// Before running we need to check if everything is in place.
// If something went wrong, we will display friendly alert.
$ok = require_once __DIR__ . '/bootstrap/compatibility.php';

if ($ok) {
    // Now, we can bootstrap our theme.
    $theme = require_once __DIR__ . '/bootstrap/theme.php';

    // Autoload theme. Uses localize_template() and
    // supports child theme overriding. However,
    // they must be under the same dir path.
    (new Tonik\Gin\Foundation\Autoloader($theme->get('config')))->register();
}

function create_posttypes() {

    register_post_type('restaurants',
        array(
            'labels'       => array(
                'name'          => __('Restaurants'),
                'singular_name' => __('Restaurant')
            ),
            'public'       => true,
            'has_archive'  => false,
            'menu_icon' => 'dashicons-store'
        )
    );

    register_post_type('organizations',
        array(
            'labels'       => array(
                'name'          => __('Organizations'),
                'singular_name' => __('Organization')
            ),
            'public'       => true,
            'has_archive'  => false,
            'menu_icon' => 'dashicons-clipboard'
        )
    );

    register_post_type('service_areas',
        array(
            'labels'       => array(
                'name'          => __('Service Areas'),
                'singular_name' => __('Service Area')
            ),
            'public'       => true,
            'has_archive'  => false,
            'menu_icon' => 'dashicons-location-alt'
        )
    );
}

add_action('init', 'create_posttypes');

// disable rest api if not logged in
add_filter('rest_authentication_errors', function ($result) {
    if (!empty($result)) {
        return $result;
    }
    if (!is_user_logged_in()) {
        return new WP_Error('rest_not_logged_in', 'You are not currently logged in.', array('status' => 401));
    }
    return $result;
});

add_filter( 'ninja_forms_render_options', function($options,$settings){
    if( $settings['label'] == 'Service Area' ){
        $args = array(
            'post_type' => 'service_areas',
            'order' => 'ASC',
            'posts_per_page' => 100,
            'post_status' => 'publish'
        );
        $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ){
            global $post;
            while ( $the_query->have_posts() ){
                $the_query->the_post();
                $options[] = array('label' => get_the_title( ), 'value' => get_the_title( ));
            }
            wp_reset_postdata();
        }
    }
    return $options;
},10,2);
